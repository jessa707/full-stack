// Section event Listener
const txt_first_name = document.querySelector("#txt-first-name");
const txt_last_name = document.querySelector("#txt-last-name");
const span_full_name = document.querySelector(".span-full-name");

// Event listener for First Name input
txt_first_name.addEventListener('input', () => {
  span_full_name.innerHTML = txt_first_name.value + ' ' + txt_last_name.value;
});

// Event listener for Last Name input
txt_last_name.addEventListener('input', () => {
  span_full_name.innerHTML = txt_first_name.value + ' ' + txt_last_name.value;
});
